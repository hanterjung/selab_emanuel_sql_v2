/**
 * Created by khan on 2016-04-22.
 */
$(document).ready(function () {
    $('#inquireForm').hide();
    $('#clusterForm').hide();

    setForms();
});

var tokenize_result = {};
var is_convert_menu = true;

function setForms() {
    $('#btnSavePronoun').click(function() {
        savePronoun($('#convertForm'));
    });

    $('#btnSaveDB').click(function() {
        var converted_sentences = [];
        for (var sentNum=0; sentNum<tokenize_result['roles'].length; sentNum++) {
            var roles = {};
            for (var role in tokenize_result['roles'][sentNum]) {
                var tokenIDs = tokenize_result['roles'][sentNum][role];
                if (tokenIDs == null) {
                    roles[role] = null;
                } else if(!(tokenIDs instanceof Array)) {
                    roles[role] = tokenIDs;
                } else {
                    roles[role] = [];
                    for (var i=0; i<tokenIDs.length; i++) {
                        var tokenId = tokenIDs[i];
                        roles[role].push(tokenize_result['tokens'][sentNum][tokenId][0]);
                    }
                }
            }
            converted_sentences.push(roles);
        }

        $.ajax("/api/convert", {
            method: 'POST',
            data: JSON.stringify({
                action: 'save',
                sentences: tokenize_result['sentences'],
                converted_sentences: converted_sentences,
            }),
            dataType: 'json',
            success: function (res) {
                $.LoadingOverlay('hide');
                if (res['code'] == 'SUCCESS') {
                    showConvert();
                } else {
                    openAlertModal('failed');
                }
            }
        });
    });


    $('#btnConvert').click(function() {
        convertSentences();
    });

    $('#btnQueryAccept').click(function() {
        $.LoadingOverlay('show');

        $('#convertForm').find('.convertedSentencesContainer')
                .find('.tokenContainer').find('.tokenDiv:not(.ex-token)').each(function() {
            var sentNum = $(this).data('sentNum');
            var tokenNum = $(this).data('tokenNum');
            var token = tokenize_result['tokens'][sentNum][tokenNum];
            token[3] = $(this).find('.selectElem').val();
            if (token[3]=="None") token[3] = null;
            token[1] = $(this).find('.selectTag').val();
        });

        var dependencies = [];
        //$('.relationPronoun').each(function() {
        //    var from = $(this).data();
        //    var to = {
        //        sentenceNum: parseInt($(this).find('select.relationSentence').val()),
        //        wordPos: parseInt($(this).find('select.relationElement').val())
        //    };
        //
        //    if (to['sentenceNum'] < 0 || to['wordPos'] < 0)
        //        return;
        //
        //    dependencies.push([
        //        [from['sentenceNum'], from['wordPos']],
        //        [to['sentenceNum'], to['wordPos']]
        //    ]);
        //});
        $('.relationPronoun:not(.ex-relationPronoun)').each(function() {
            var from = {
                sentenceNum: parseInt($(this).find('select.fromRelationSentence').val()),
                elementNum: parseInt($(this).find('select.fromRelationElement').val())
            };
            var to = {
                sentenceNum: parseInt($(this).find('select.toRelationSentence').val()),
                elementNum: parseInt($(this).find('select.toRelationElement').val())
            };
            from['role'] = tokenize_result['tokens'][from.sentenceNum][from.elementNum][3];
            to['role'] = tokenize_result['tokens'][to.sentenceNum][to.elementNum][3];

            dependencies.push([
                [from.sentenceNum, from.elementNum, from.role],
                [to.sentenceNum, to.elementNum, to.role]
            ]);
        });

        console.log(dependencies);

        $.ajax("/api/convert", {
            method: 'POST',
            data: JSON.stringify({
                action: 'save',
                tokens: tokenize_result['tokens'],
                sentences: tokenize_result['sentences'],
                section: tokenize_result['section'],
                dependencies: dependencies
            }),
            dataType: 'json',
            success: function (res) {
                $.LoadingOverlay('hide');
                if (res['code'] == 'SUCCESS') {
                    $('#btnQueryAccept').attr('disabled', 'disabled');
                    $('#btnQueryRefine').attr('disabled', 'disabled');
                    $('#btnQueryClear').text('Clear');
                    setRefineConvertResult(false);
                    openAlertModal("The convert result is saved successfully.", "Save Success")
                } else {
                    openAlertModal('failed');
                }
            }
        });
    });

    $('#btnInquire').click(function() {
        if (inquiryMode == 'type') {
            var sqlQuery = $('#inputSQLQuery').val();
            $.LoadingOverlay('show');
            $.ajax("/api/rawquery", {
                method: 'POST',
                data: JSON.stringify({
                    query: sqlQuery
                }),
                dataType: 'json',
                success: function(res) {
                    console.log(res);
                    $.LoadingOverlay('hide');
                    if(res['code'] == 'SUCCESS') {
                        setTypeSQLQueryTable(res['result']);
                    } else {
                        openAlertModal('Query Syntax Error.');
                    }
                }
            });
        } else {
            var selectQuery = makeSelectSQLQuery($('#inquireSQL'));
            console.log(selectQuery);
            console.log(JSON.stringify(selectQuery));

            if (checkValidQuery(selectQuery)) {
                var selectQueryString = makeQueryStringFromSelectSQLQuery(selectQuery);
                console.log(selectQueryString);
                selectQueryString = 'SELECT * from ConvertedSentence WHERE ' + selectQueryString;

                $.LoadingOverlay('show');
                $.ajax("/api/rawquery", {
                method: 'POST',
                data: JSON.stringify({
                    query: selectQueryString
                }),
                dataType: 'json',
                success: function(res) {
                    console.log(res);
                    $.LoadingOverlay('hide');
                    if(res['code'] == 'SUCCESS') {
                        setTypeSQLQueryTable(res['result']);
                    } else {
                        openAlertModal('Query Syntax Error.');
                    }
                }
            });
            } else {
                openAlertModal('Please fill out complete query', 'Invalid Select Query');
            }
        }
    });

    $('#btnQueryClear, #btnConvertCancel').click(function() {
        tokenize_result = {};
        showConvert();
    });
    $('#btnQueryRefine').click(function() {
        setRefineConvertResult();
    });

    $('#btnInquireCancel, #btnInquireClear').click(function() {
        tokenize_result = {};
        showInquire();
    });

    $('.btn-dbtable').click(function() {
        $('.btn-dbtable').removeClass('selected');
        $(this).addClass('selected').blur();

        var tableName = $(this).data('tablename');

        $.LoadingOverlay('show');
        $.ajax("/api/dbtable", {
            method: 'POST',
            data: JSON.stringify({
                table: tableName
            }),
            dataType: 'json',
            success: function (res) {
                console.log(res);
                $.LoadingOverlay('hide');
                if (res['code'] == 'SUCCESS') {
                    var table = $('#dbForm table.dbTable');
                    var tableData = res['table'];
                    var columns = res['columns'];
                    setSQLTable(table, tableData, columns);
                }
            }
        });
    });

    //$('#inputSection').change(function() {
    //    var value = $(this).val();
    //    if(value !='' && value !=null) {
    //        if (value < 1) $(this).val(null);
    //        else if (value > 99999) $(this).val(null);
    //    }
    //});

    showConvert();
    $('.ex-convertedSentences').hide();
    $('.ex-token').hide();
    $('.ex-tr').hide();
    $('.ex-formalExpression').hide();
    $('.ex-answerContainer').hide();
    $('.ex-relationPronoun').hide();
    $('.ex-inquireSQLNode').hide();
    $('.ex-addElem').hide();
    $('.ex').hide();
    $('.ex-pronounSentence').hide();
    $('.ex-pronounToken').hide();
};

function convertSentences() {
    tokenize_result = {};
    var sentences = $('#inputOriginSentences').val();

    if (!/\S/.test(sentences)) {
        openAlertModal("There is no sentence.");
        return;
    }

    $.LoadingOverlay('show');
    $.ajax("/api/convert", {
        method: 'POST',
        data: JSON.stringify({
            action: 'tokenize',
            sentences: sentences
        }),
        dataType: 'json',
        success: function (res) {
            console.log(res);
            console.log(JSON.stringify(res));

            $.LoadingOverlay('hide');
            if (res['code'] == 'SUCCESS') {
                //range to list
                var pronouns = res['pronouns'];
                for (var sentNum=0; sentNum < res['pronouns'].length; sentNum++) {
                    for (var i=0; i<res['pronouns'][sentNum].length; i++) {
                        if (res['pronouns'][sentNum][i].length == 2) {
                            var start = res['pronouns'][sentNum][i][0];
                            var end = res['pronouns'][sentNum][i][1];
                            var ranges = [];
                            for (var k = start; k <= end; k++) {
                                ranges.push(k);
                            }
                            res['pronouns'][sentNum][i] = ranges;
                        }
                    }
                }
                tokenize_result['tokens'] = res['tokens'];
                tokenize_result['sentences'] = res['sentences'];
                tokenize_result['roles'] = res['roles'];
                tokenize_result['pronouns'] = res['pronouns'];
                tokenize_result['nouns'] = res['nouns'];

                //console.log(res['tokens']);
                //console.log(res['sentences']);

                var convertForm = $('#convertForm');
                resetAnalyzedForms(convertForm);
                setAnalyzedSents(convertForm, res['tokens'], res['sentences'],
                    res['roles'],res['pronouns'], res['nouns']);
                // setSenElTables(convertForm, res['tokens'], res['sentences']);
                $('#inputOriginSentences, #inputSection').attr('disabled', 'disabled');
                //convertForm.find('.convertBtnDiv').hide();
                convertForm.find('.convertBtnDiv').find('button').attr('disabled', 'disabled');
                convertForm.find('.acceptBtnsDiv').show();
                $('#convertQueryButtonContainer').show();
                console.log($('#convertQueryButtonContainer'));
            } else {
                openAlertModal('failed');
            }
        }
    });
}

function resetAnalyzedForms(form) {
    form.find('.convertedSentencesContainer .table-analyzed-sentence .tr-analyzed-sentence').each(function (index, elem) {
        if (index == 0) return;
        $(elem).remove();
    });
    form.find('.convertedSentencesContainer .analyzed-sentence-info .pronounSentence').each(function (index, elem) {
        if (index == 0) return;
        $(elem).remove();
    });

    $('#btnSavePronoun').show();
    $('#btnSaveDB').hide();

    form.find('.convertedSentencesRow').hide();

    // form.find('.relationPronounContainer .relationPronoun').each(function (index, elem) {
    //     if (index == 0) return;
    //     $(elem).remove();
    // });
    // form.find('.formalExpression').each(function (index, elem) {
    //     if (index == 0) return;
    //     $(elem).remove();
    // });
    // // form.find('.convertedSentencesRow').hide();
    // form.find('.acceptBtnsDiv').hide();
    // //form.find('.convertBtnDiv').show();
    // form.find('.convertBtnDiv').find('button').removeAttr('disabled');
}


function setAnalyzedSents(form, tokensList, sentenceList, rolesList, pronounList, nounList) {
    form.find('.convertedSentencesRow').show();
    for (var i=0; i<tokensList.length; i++) {
        addAnalyzedSentsRoles(form.find('.convertedSentencesRow table.table-analyzed-sentence'),
            tokensList[i], rolesList[i]);
    }
    var hasPronoun = false;
    if (pronounList != null) {
        for (var i=0; i<tokensList.length; i++) {
            if (pronounList[i].length > 0) {
                addAnalyzeSentsPronouns(form.find('.convertedSentencesRow .analyzed-sentence-info'),
                    i, tokensList[i], sentenceList[i], rolesList[i], pronounList[i], nounList);
                hasPronoun = true;
            }
        }
    }
    if (hasPronoun)  {
        form.find('.analyzed-sentence-info').show();
        $('#btnSavePronoun').show();
        $('#btnSaveDB').hide();

        $('.pronounTokenReplacement').on('input', function(e) {
            el = e.target;
            $(el).prev('select').val('__NO_VALUE__');
        });

        $('.pronounTokenSelect').on('change', function(e) {
            if ($(this).value != '__NO_VALUE__') {

                $(this).next('input').val('');
            }
        });
    } else {
        form.find('.analyzed-sentence-info').hide();
        $('#btnSavePronoun').hide();
        $('#btnSaveDB').show();
    }
}

function addAnalyzedSentsRoles(table, tokens, roles) {
    var sentRolesRow = table.find('.tr-analyzed-sentence.ex-tr').clone()
        .removeClass('ex-tr').show();
    for (var role in roles) {
        var tokenIDs = roles[role];
        if (tokenIDs == undefined || tokenIDs == null) {
            tokenIDs = [];
        }

        var tokenText = "";
        if (!(tokenIDs instanceof Array)) {
            tokenText = tokenIDs
        } else {
            tokenText = "";
            for (var i = 0; i < tokenIDs.length; i++) {
                if (i > 0) tokenText += ' ';
                tokenText += tokens[tokenIDs[i]][0];
            }
        }
        sentRolesRow.find('> .td-as-' + role).text(tokenText);
        sentRolesRow.appendTo(table.find('tbody'));
    }
}

function addAnalyzeSentsPronouns(form, sentNum, tokens, sentence, roles, pronouns, nouns) {
    var pronounSentence = form.find('.pronounSentence.ex-pronounSentence').clone()
        .removeClass('ex-pronounSentence').show();
    pronounSentence.attr('id', 'pronounSentence-' + sentNum);
    pronounSentence.data('sentNum', sentNum);
    var allPronounTokenIDs = [];
    for (var i=0; i<pronouns.length; i++) {
        var pronoun = pronouns[i];
        var pronounToken = pronounSentence.find('.pronounToken.ex-pronounToken').clone()
            .removeClass('ex-pronounToken').show().css('display', 'inline-block');
        var pronounTokenText = "";
        for (var k=0; k<pronoun.length; k++) {
            allPronounTokenIDs.push(pronoun[k]);
            if (k > 0) pronounTokenText += ' ';
            pronounTokenText += tokens[pronoun[k]][0];
        }
        pronounToken.find('.pronounTokenOriginal').text(pronounTokenText);
        pronounToken.appendTo(pronounSentence.find('.pronounTokenDiv'));
    }
    console.log(sentNum + ": " + allPronounTokenIDs);
    console.log(tokens);
    var sentenceHTML = '';
    for (var i=0; i<tokens.length; i++) {
        if (i>0 && tokens[i][1]!=null) sentenceHTML += ' ';
        if ($.inArray(i, allPronounTokenIDs)>=0) {
            console.log(i + "(" + allPronounTokenIDs + ")");
            sentenceHTML += "<strong>"+tokens[i][0]+"</strong>"
        } else {
            sentenceHTML += tokens[i][0];
        }
    }
    pronounSentence.find("> .pronounSentHeader .sentNum").text(sentNum+1);
    pronounSentence.find("> .pronounSentHeader .sentText").html(sentenceHTML);
    pronounSentence.appendTo(form.find('.pronounSentenceDiv'));
    $('select.pronounTokenSelect option:not(:disabled)').remove();
    for (var i = 0; i < nouns.length; i++) {
        $('select.pronounTokenSelect').append("<option>"+nouns[i]+"</option>");
    }
}

function isSame(array1, array2) {
    if (array1 == null || array2 == null) return false;
    console.log("MK ---------------- isSame");
    console.log(array1);
    console.log(array2);
    if (!(array1 instanceof Array && array2 instanceof Array)) return false;
    return (array1.length == array2.length) && array1.every(function(element, index) {
            return element === array2[index];
        });
}

function savePronoun(form) {
    var pronouns = tokenize_result['pronouns'];
    var roles = tokenize_result['roles'];
    var $pronounTokens = $(form).find('.convertedSentencesRow .analyzed-sentence-info .pronounToken:not(.ex-pronounToken)');
    var pronounCnt = 0;
    var $rows = $('.table-analyzed-sentence tbody tr');
    console.log('MK ---------------------------');
    console.log(pronouns);
    // {subject, verb, complement, object, sp, tp. mp, oc, ac, gc, wh, cj, object2}
    for (var i = 0; i < pronouns.length; i++) {
        for (var j = 0; j < pronouns[i].length; j++) {
            var pronounWList = pronouns[i][j];
            console.log('MK ---------------------------');
            console.log(pronounWList);

            var $pronounToken = $($pronounTokens[pronounCnt++]);
            var tokenOriginal = $pronounToken.find('.pronounTokenOriginal').text();
            var tokenReplacement = $pronounToken.find('.pronounTokenReplacement').val();
            var $tokenSelect = $pronounToken.find('.pronounTokenSelect');

            if (tokenReplacement == null || !/\S/.test(tokenReplacement)) {
                var $optionSelected = $tokenSelect.find('option:selected');
                if ($optionSelected.val() != '__NO_VALUE__') {
                    tokenReplacement = $optionSelected.text();
                } else {
                    tokenReplacement = tokenOriginal;
                }
            } else {

            }

            for (var k in roles[i]) {
                if (isSame(roles[i][k], pronounWList)) {
                    console.log('MK -----------------role----------');
                    console.log(i + ' ' + k);
                    roles[i][k] = tokenReplacement;
                }
                else {
                    for (var m = 0; m < pronounWList.length; m++) {
                        if (roles[i][k] != null) {
                            var idx = roles[i][k].indexOf(pronounWList[m]);
                            if (idx >= 0) {
                                var replaceStarted = false;
                                var tokenReplacementFull = '';
                                for (var n = 0; n < roles[i][k].length; n++) {
                                    if (replaceStarted) {
                                        if (pronounWList.indexOf(roles[i][k][n]) == -1) {
                                            tokenReplacementFull += ' ' + tokenize_result['tokens'][i][roles[i][k][n]][0];
                                            replaceStarted = false;
                                            console.log('MK -----------------role---------- partial 1');
                                            console.log(tokenize_result['tokens'][i][roles[i][k][n]]);
                                        }
                                    } else {
                                        console.log('MK -----------------role---------- partial 0000');
                                        console.log(roles[i][k][n]);
                                        console.log(pronounWList);
                                        console.log(tokenize_result['tokens']);
                                        if (pronounWList.indexOf(roles[i][k][n]) >= 0) {
                                            replaceStarted = true;
                                            tokenReplacementFull += ' ' + tokenReplacement;
                                        } else {
                                            var w = tokenize_result['tokens'][i][roles[i][k][n]][0];
                                            tokenReplacementFull += ' ' + tokenize_result['tokens'][i][roles[i][k][n]][0];
                                            console.log('MK -----------------role---------- partial 1');
                                            console.log(tokenize_result['tokens'][i][roles[i][k][n]]);
                                        }
                                    }
                                }
                                console.log('MK -----------------role---------- partial');
                                console.log(tokenReplacementFull);
                                roles[i][k] = tokenReplacementFull;
                                break;
                            }
                        }
                    }
                }
            }
        }
    }
    console.log('MK -----------------roles----------');
    console.log(roles);
    var convertForm = $('#convertForm');
    resetAnalyzedForms(convertForm);
    setAnalyzedSents(convertForm, tokenize_result['tokens'], tokenize_result['sentences'],
                    roles,null, tokenize_result['nouns']);


    // var bSuccess = true;
    // form.find('.convertedSentencesRow .analyzed-sentence-info .pronounToken:not(.ex-pronounToken) .pronounTokenReplacement').each(function(index, elem) {
    //     var replaceTokenInput = $(elem);
    //     var text = replaceTokenInput.val();
    //     if (text == null || !/\S/.test(text)) {
    //         bSuccess = false;
    //     }
    // });

    // if (!bSuccess) {
    //     openAlertModal("Fill pronoun fields", "Warning!");
    //     return
    // }

    // form.find('.convertedSentencesRow .analyzed-sentence-info .pronounSentence:not(.ex-pronounSentence)').each(function (index, elem) {
    //     elem = $(elem);
    //     var sentNum = elem.data('sentNum');
    //     var newSentText = "";
    //     elem.find('.pronounToken:not(.ex-pronounToken)').each(function(pronounIndex, elem) {
    //         var pronouns = tokenize_result['pronouns'][sentNum][pronounIndex];
    //         for (var i=0; i<pronouns.length; i++) {
    //             tokenize_result['tokens'][sentNum][pronouns[i]] = null;
    //         }
    //         tokenize_result['tokens'][sentNum][pronouns[0]] = $(elem).find('.pronounTokenReplacement').val();
    //     });
    //     // console.log(tokenize_result['tokens'][sentNum]);
    //     for (var i=0; i<tokenize_result['tokens'][sentNum].length; i++) {
    //         var token = tokenize_result['tokens'][sentNum][i];
    //         if (token == null) continue;
    //         if (i>0 && (token==null || token[1]!=null)) newSentText += ' ';
    //         if (typeof token == 'string' || token instanceof String) {
    //             newSentText += token;
    //         } else {
    //             newSentText += token[0];
    //         }
    //         // console.log(i + ", " + newSentText);
    //     }
    //
    //     tokenize_result['sentences'][sentNum] = newSentText;
    //     // console.log(tokenize_result);
    //     // console.log(newSentText);
    // });
    //
    // var newSentences = "";
    // for (var i=0; i<tokenize_result['sentences'].length; i++) {
    //     if (i>0) newSentences += ' ';
    //     newSentences += tokenize_result['sentences'][i];
    // }
    //
    // form.find('.sentenceInputRow .text-area').val(newSentences);
    // resetAnalyzedForms(form);
    // convertSentences();
}

function setSenElTables(form, tokensList, sentences) {
    form.find('.convertedSentencesRow').show();
    for (var i=0; i<tokensList.length; i++) {
        var tokens = tokensList[i];
        addSenElTable(form, tokens, sentences[i], i);
    }
    setSenElRelation(form, tokensList);
}

function addSenElTable(form, tokens, sentence, num) {
    var newSenTable = form.find('.convertedSentencesContainer .ex-convertedSentences').clone()
        .removeClass('ex-convertedSentences').show();
    //console.log(newSenTable);
    newSenTable.appendTo(form.find('.convertedSentencesContainer .convertedSentencesDiv'));

    if(is_convert_menu) {
        newSenTable.find('.sentenceNum').text('Sentence #' + (num + 1) + ': ');
    } else {
        newSenTable.find('.sentenceNum').text('Inquiry Sentence: ');
    }
    newSenTable.find('.sentenceText').text(sentence);
    // var table = newSenTable.find('table');
    var tokenContainer = newSenTable.find('.tokenContainer');

    for (var i=0; i<tokens.length; i++) {
        // addSenElRow(table, tokens[i]);
        addTokenEl(tokenContainer, tokens, num, i);
    }
}

function addTokenEl(container, tokens, sentenceNum, tokenNum) {
    var token = tokens[tokenNum];
    var word = token[0];
    var langTag = token[1];
    var langElem = token[3];


    var tokenDiv = container.find('.ex-token').clone().removeClass('ex-token')
        .show().css('display', 'inline-block');
    $(tokenDiv).data('sentNum', sentenceNum);
    $(tokenDiv).data('tokenNum', tokenNum);
    tokenDiv.appendTo(container);

    tokenDiv.find('.inputToken').text(word);
    tokenDiv.find('.selectTag').val(langTag);
    if (langElem == null || langElem == undefined) langElem = 'None';
    tokenDiv.find('.selectElem').val(langElem);

}

function setSenElRelation(form, tokensList) {
    form.find('.relationPronoun:not(.ex-relationPronoun)').remove();
    form.find('.relationPronounContainer').show();

    form.find('#btnRelationAdd').unbind('click').off('click').click(function() {
        var relCount = form.find('.relationPronoun:not(.ex-relationPronoun)').length;
        var newRelation = form.find('.relationPronounContainer .ex-relationPronoun').clone()
            .removeClass('ex-relationPronoun').show();
        newRelation.insertBefore(form.find('.relationPronounDiv .relationPronounButton'));

        newRelation.find('.relationHeader').text('Dependency #' + (relCount+1));
        for (var sentNum=0; sentNum<tokensList.length; sentNum++) {
            newRelation.find('.fromRelationSentence').append($('<option>', {
                text : sentNum+1,
                value : sentNum
            }));
            newRelation.find('.toRelationSentence').append($('<option>', {
                text : sentNum+1,
                value : sentNum
            }));
        }

        newRelation.find('.relationSentence').change(function() {
            var selectRelElem = $(this).parent().parent().find('select.relationElement');
            selectRelElem.empty();

            var sentNum = parseInt($(this).val());
            var tokens = tokensList[sentNum];

            for(var wordPos=0; wordPos<tokens.length; wordPos++) {
                selectRelElem.append($('<option>', {
                    text : tokens[wordPos][0],
                    value : wordPos
                }));
            }
        });
        newRelation.find('.relationSentence').change();

        newRelation.find('.removeRelation').click(function() {
            newRelation.remove();
            form.find('.relationPronoun:not(.ex-relationPronoun)').each(function(index, elem) {
                $(elem).find('.relationHeader').text('Dependency #' + (index+1));
            });
        });

        $(this).blur();
    });
}

function resetAnswerForm() {
    $('#btnInquire').removeAttr('disabled');
    $('#btnInquireCancel').removeAttr('disabled');
    $('#btnInquireClear').removeAttr('disabled');
    $('.inquireSQL input, .inquireSQL select').removeAttr('readonly');
    $('.answerSentenceContainer').hide();
    $('.typeQueryTableContainer').hide();
    $('.answerResultButtonContainer').hide();
    $('#inputSQLQuery').val('');
    $('.answerContainer').each(function (index, elem) {
        if (index == 0) return;
        $(elem).remove();
    });

    inquireSQL.find('.inquireSQLElem, .addElem').each(function (index, elem) {
        var el = $(elem);
        if(el.hasClass('ex')) return;
        try {
            el.remove();
        } catch(e) {}
    });
    createInquirySQLAddButton().appendTo(inquireSQL);
}

function setAnswers(answers, type) {
    $('#btnInquire').attr('disabled', 'disabled');
    $('#btnInquireCancel').attr('disabled', 'disabled');
    $('.inquireSQL input, .inquireSQL select').attr('readonly', 'readonly');
    $('.answerSentenceContainer').show();
    $('.answerResultButtonContainer').show();
    setRefineConvertResult(false);

    var answerForm = $('.answerContainer');

    var answerText = '';
    if(answers == undefined || answers ==null || answers.length == 0) {
        answerText = 'There is no Answer.';
    } else {
        for (var i = 0; i < answers.length; i++) {
            //if (i >= 5) break;

            var answer = answers[i];
            answerText += answer['sentence'] + ' (' + type + ': ' + answer['text'] + ')<br/>';
        }
    }
    answerForm.find('.answerArea').html(answerText);
}

function setTypeSQLQueryTable(tableData) {
    $('#btnInquire').attr('disabled', 'disabled');
    $('#btnInquireCancel').attr('disabled', 'disabled');
    $('#inputSQLQuery').attr('readonly', 'readonly');
    $('.typeQueryTableContainer').show();
    $('.answerResultButtonContainer').show();
    var table = $('#inquireForm table.dbTable');

    console.log(tableData);
    setSQLTable(table, tableData);
}

var inquiryMode = 'type';
function setInquiryTypeSQL() {
    resetAnswerForm();
    resetSQLTable($('#inquireForm table.dbTable'));

    $('#btnSelectSQL').removeClass('selected');
    $('#btnTypeSQL').addClass('selected');
    $('#inquirySelectSQL').hide();
    $('#inputSQLQuery').show();
    inquiryMode = 'type';
}

var inquireSQL = $('#inquireSQL');
var inquireSQLNode = inquireSQL.find('.ex-inquireSQLNode').clone()
    .removeClass('ex-inquireSQLNode').removeClass('ex').show().css('display', 'inline-block');
var inquireSQLGroup = $('<div class="inquireSQLElem inquireSQLElemGroup"></div>');
var inquireSQLOperatorAnd = $('<div class="inquireSQLElem inquireSQLOperator">AND</div>');
var inquireSQLOperatorOr = $('<div class="inquireSQLElem inquireSQLOperator">OR</div>');
var inquireSQLBracketLeft = $('<div class="inquireSQLBracket inquireSQLBracket-left">(</div>');
var inquireSQLBracketRight = $('<div class="inquireSQLBracket inquireSQLBracket-right">)</div>');
var inquireSQLAddButton = inquireSQL.find('.ex-addElem').clone()
    .removeClass('ex-addElem').removeClass('ex').show().css('display', 'inline-block');

function setInquirySelectSQL() {
    resetAnswerForm();
    resetSQLTable($('#inquireForm table.dbTable'));

    $('#btnTypeSQL').removeClass('selected');
    $('#btnSelectSQL').addClass('selected');
    $('#inputSQLQuery').hide();
    $('#inquirySelectSQL').show();
    inquiryMode = 'select';

    inquireSQL.find('.inquireSQLElem, .addElem').each(function (index, elem) {
        var el = $(elem);
        if(el.hasClass('ex')) return;
        try {
            el.remove();
        } catch(e) {}
    });

    createInquirySQLAddButton().appendTo(inquireSQL);
}

function createInquirySQLNode() {
    var node = inquireSQLNode.clone();
    node.find('.removeElem').click(function() {
        var parent = node.parent();
        if (parent.find('> .inquireSQLNode:not(.ex), > .inquireSQLElemGroup').length > 1) {
            var prev = node.prev(':not(.ex)');
            var next = node.next(':not(.ex)');
            if (prev.hasClass('inquireSQLOperator')) {
                prev.remove();   //remove operator
            } else if (prev.length == 0 && next.hasClass('inquireSQLOperator')) {
                next.remove();
            }
            node.remove();
        } else {
            node.remove();
        }
    });
    return node;
}

function createInquirySQLAddButton() {
    var addButton = inquireSQLAddButton.clone();
    addButton.click(function() {
        var parent = addButton.parent();
        if (parent.find('> .inquireSQLElem:not(.ex)').length > 0) {
            addButton.find('.dropdown-menu .whenEmpty').hide();
            addButton.find('.dropdown-menu .whenFill').show();
        } else {
            addButton.find('.dropdown-menu .whenFill').hide();
            addButton.find('.dropdown-menu .whenEmpty').show();
        }
        if (parent.hasClass('inquireSQLElemGroup')) {
            addButton.find('.whenParentGroup').show();
        } else {
            addButton.find('.whenParentGroup').hide();
        }
    });
    addButton.find('.addElemElem').click(function() {
        addButton.before(createInquirySQLNode());
    });
    addButton.find('.addElemGroup').click(function() {
        var group = inquireSQLGroup.clone();
        addButton.before(group);
        inquireSQLBracketLeft.clone().appendTo(group);
        createInquirySQLAddButton().appendTo(group);
        inquireSQLBracketRight.clone().appendTo(group);
    });
    addButton.find('.addElemAndElem').click(function() {
        addButton.before(inquireSQLOperatorAnd.clone());
        addButton.before(createInquirySQLNode());
    });
    addButton.find('.addElemOrElem').click(function() {
        addButton.before(inquireSQLOperatorOr.clone());
        addButton.before(createInquirySQLNode());
    });
    addButton.find('.addElemAndGroup').click(function() {
        addButton.before(inquireSQLOperatorAnd.clone());
        var group = inquireSQLGroup.clone();
        addButton.before(group);
        inquireSQLBracketLeft.clone().appendTo(group);
        createInquirySQLAddButton().appendTo(group);
        inquireSQLBracketRight.clone().appendTo(group);
    });
    addButton.find('.addElemOrGroup').click(function() {
        addButton.before(inquireSQLOperatorOr.clone());
        var group = inquireSQLGroup.clone();
        addButton.before(group);
        inquireSQLBracketLeft.clone().appendTo(group);
        createInquirySQLAddButton().appendTo(group);
        inquireSQLBracketRight.clone().appendTo(group);
    });
    addButton.find('.addElemRemoveGroup').click(function() {
        var group = addButton.parent();
        var parent = group.parent();
        if (parent.find('> .inquireSQLNode:not(.ex), > .inquireSQLElemGroup').length > 1) {
            var prev = group.prev(':not(.ex)');
            var next = group.next(':not(.ex)');
            if (prev.hasClass('inquireSQLOperator')) {
                prev.remove();   //remove operator
            } else if (prev.length == 0 && next.hasClass('inquireSQLOperator')) {
                next.remove();
            }
            group.remove();
        } else {
            group.remove();
        }
    });
    return addButton;
}

function makeSelectSQLQuery(sqlElemGroup) {
    var selectQuery = [];

    var sqlElem = { operator: null };
    sqlElemGroup.find('> .inquireSQLElem:not(.ex)').each(function(index, elem) {
        if ($(elem).hasClass('inquireSQLOperator')) {
            selectQuery.push(sqlElem);
            sqlElem = { operator: $(elem).text() };
        } else if ($(elem).hasClass('inquireSQLNode')) {
            sqlElem['elem'] = {
                text: $(elem).find('input.elemText').val(),
                role: $(elem).find('select.elemRole').val()
            };
        } else if ($(elem).hasClass('inquireSQLElemGroup')) {
            sqlElem['group'] = makeSelectSQLQuery($(elem));
        }
    });
    selectQuery.push(sqlElem);

    return selectQuery;
}

function checkValidQuery(selectQuery) {
    if (selectQuery==undefined || selectQuery==null || selectQuery.length==0)
        return false;
    for (var i=0; i<selectQuery.length; i++) {
        if (i > 0 && selectQuery[i]['operator']==undefined && selectQuery[i]['operator']==null ) {
            return false;
        }
        if ('elem' in selectQuery[i]) {
            var text = selectQuery[i]['elem']['text'];
            var role = selectQuery[i]['elem']['role'];
            if (text == null || !/\S/.test(text)) return false;
            if (role == null || !/\S/.test(role)) return false;
        } else if ('group' in selectQuery[i]) {
            if (checkValidQuery(selectQuery[i]['group']) == false)
                return false;
        } else {
            return false;
        }
    }
    return true;
}

function makeQueryStringFromSelectSQLQuery(selectQuery) {
    var strQuery = '';
    for (var i=0; i<selectQuery.length; i++) {
        if (i > 0) {
            strQuery += ' ' + selectQuery[i]['operator'] + ' ';
        }
        if('elem' in selectQuery[i]) {
            strQuery += selectQuery[i]['elem']['role'] + '=\'' + selectQuery[i]['elem']['text'] + '\'';
        } else if ('group' in selectQuery[i]) {
            strQuery += '(' + makeQueryStringFromSelectSQLQuery(selectQuery[i]['group']) + ')';
        }
    }
    return strQuery;
}

function setSQLTable(tableElem, tableData, columns) {
    resetSQLTable(tableElem);

    if (columns != null && columns != undefined) {
        var $tr = $('<tr></tr>');
        for (var i = 0; i < columns.length; i++) {
            var $td = $('<td></td>');
            $td.html(columns[i]);
            $tr.append($td);
        }
        tableElem.find('thead').append($tr);
    }

    for (var i = 0; i < tableData.length; i++) {
        var $tr = $('<tr></tr>');
        for (var j = 0; j < tableData[i].length; j++) {
            var $td = $('<td></td>');
            $td.html(tableData[i][j]);
            $tr.append($td);
        }
        tableElem.find('tbody').append($tr);
    }


    //
    // if (tableData == undefined || tableData == null || tableData.length == 0) {
    //     tableElem.find('tbody').append('<tr><td>No Data</td></tr>');
    //     return;
    // }
    //
    // var tableKeys = Object.keys(tableData[0]);
    // var hasIdColumn = false;
    // if ('id' in tableData[0]) {
    //     hasIdColumn = true;
    // }
    //
    // var htmlString = '<tr>';
    // if (hasIdColumn) htmlString+='<th>id</th>';
    // for (var k=0; k<tableKeys.length; k++) {
    //     if(tableKeys[k] == 'id') continue;
    //     htmlString += '<th>' + tableKeys[k] + '</th>';
    // }
    // tableElem.find('thead').append(htmlString);
    //
    // console.log(tableData);
    // for (var i=0; i<tableData.length; i++) {
    //     htmlString = '<tr>';
    //     if (hasIdColumn) htmlString += '<td>'+tableData[i]['id']+'</td>';
    //     for (var k=0; k<tableKeys.length; k++) {
    //         if(tableKeys[k] == 'id') continue;
    //         var text = tableData[i][tableKeys[k]];
    //         if (text == undefined || text == null) {
    //             text = ''
    //         }
    //         htmlString += '<td>' + text + '</td>'
    //     }
    //     htmlString += '</tr>';
    //     tableElem.find('tbody').append(htmlString);
    // }
}

function resetSQLTable(tableElem) {
    tableElem.find('thead').empty();
    tableElem.find('tbody').empty();
}

function showConvert() {
    is_convert_menu = true;
    tokenize_result = {};
    $('#convertForm').show();
    $('#inquireForm').hide();
    $('#dbForm').hide();
    resetAnalyzedForms($('#convertForm'));
    $('#convertQueryButtonContainer').hide();
    $('#convertForm').find('input, textarea').val('');
    $('#convertForm').find('input, textarea').removeAttr('disabled');
    $('#btnQueryAccept').removeAttr('disabled');
    $('#btnQueryRefine').removeAttr('disabled');
    $('#btnQueryClear').text('Cancel');
    $('.row-menu').find('.btn-menu').removeClass('selected').blur();
    $('#btnConvertMenu').addClass('selected');
    $('#btnRelationAdd').removeAttr('disabled');
}

function showInquire() {
    is_convert_menu = false;
    tokenize_result = {};
    $('#convertForm').hide();
    $('#inquireForm').show();
    $('#dbForm').hide();
    resetAnalyzedForms($('#inquireForm'));
    $('#answerButtonContainer').hide();
    $('#inquireForm').find('input, textarea').val('');
    $('#inquireForm').find('input, textarea').removeAttr('disabled');
    resetAnswerForm();
    resetSQLTable($('#inquireForm table.dbTable'));
    $('.row-menu').find('.btn-menu').removeClass('selected').blur();
    $('#btnInquireMenu').addClass('selected');
}

function showDB() {
    $('#convertForm').hide();
    $('#inquireForm').hide();
    $('#dbForm').show();
    $('#dbForm').find('.btn-dbtable').removeClass('selected');
    $('.row-menu').find('.btn-menu').removeClass('selected').blur();
    $('#btnDBMenu').addClass('selected');
    resetSQLTable($('#dbForm table.dbTable'))
}

function setRefineConvertResult(enable) {
    if (enable == undefined || enable == null)
        enable = true;
    if (enable) {
        $('.tokenContainer').find('.tokenDiv:not(.ex-token)')
            .find('.selectTag').removeAttr('disabled');
        $('.tokenContainer').find('.tokenDiv:not(.ex-token)')
            .find('.selectElem').removeAttr('disabled');
        $('#btnRelationAdd').removeAttr('disabled');
    } else {
        $('.tokenContainer').find('.tokenDiv:not(.ex-token)')
            .find('.selectTag').attr('disabled', 'disabled');
        $('.tokenContainer').find('.tokenDiv:not(.ex-token)')
            .find('.selectElem').attr('disabled', 'disabled');
        $('#btnRelationAdd').attr('disabled', 'disabled');
    }
}

function openAlertModal(msg, title) {
    if (title==undefined || title==null) {
        title = "Alert";
    }
    $('#alertModalTitle').text(title);
    $('#alertModalMsg').text(msg);
    $('#alertModal').modal('show');
}

function arraysEqual(a, b) {
  if (a === b) return true;
  if (a == null || b == null) return false;
  if (a.length != b.length) return false;

  // If you don't care about the order of the elements inside
  // the array, you should sort both arrays here.

  for (var i = 0; i < a.length; ++i) {
    if (a[i] !== b[i]) return false;
  }
  return true;
}