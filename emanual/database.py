import sqlite3


def dict_factory(cursor, row):
    d = {}
    for idx, col in enumerate(cursor.description):
        d[col[0]] = row[idx]
    return d


class DBManager(object):

    def __init__(self):
        self.conn = sqlite3.connect('emanual_db.sqlite3')
        # self.conn.row_factory = dict_factory
        print("Opened database successfully")

    def drop_tables(self):
        self.conn.execute("DROP TABLE IF EXISTS UserManual;")
        self.conn.execute("DROP TABLE IF EXISTS SourceSentence;")
        self.conn.execute("DROP TABLE IF EXISTS ConvertedSentence;")
        self.conn.commit()

    def create_tables(self):
        self.conn.execute('''CREATE TABLE IF NOT EXISTS UserManual (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            name CHAR(1000)
            )''')

        self.conn.execute('''CREATE TABLE IF NOT EXISTS SourceSentence (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            text TEXT NOT NULL,
            section CHAR(50),
            page_num INT,
            user_manual_id INT NOT NULL,
            timestamp TIMESTAMP DEFAULT CURRENT_TIMESTAMP)''')
        self.conn.execute('''CREATE INDEX SourceSentence_user_manual_id_index ON SourceSentence('user_manual_id')''')

        # Spatial Preposition
        # Temporal Preposition
        # Movement Preposition
        # Object Complement
        # Attachment Complement
        # WH Clause
        # Conjunction
        self.conn.execute('''CREATE TABLE ConvertedSentence (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            source_sentence_id INT NOT NULL,
            subject CHAR(1000),
            verb CHAR(1000),
            complement CHAR(1000),
            object CHAR(1000),
            sp CHAR(1000),
            tp CHAR(1000),
            mp CHAR(1000),
            oc CHAR(1000),
            ac CHAR(1000),
            gc CHAR(1000),
            wh CHAR(1000),
            cj CHAR(1000),
            object2 CHAR(1000),
            timestamp TIMESTAMP DEFAULT CURRENT_TIMESTAMP)''')
        self.conn.execute('''CREATE INDEX ConvertedSentence_source_sentence_id_index ON ConvertedSentence('source_sentence_id')''')

        self.conn.commit()
        print("Table created successfully")

    def save_user_manual(self, name=None):
        name, = self._convert_to_safetexts((name,))
        sql = "INSERT INTO UserManual (name) VALUES ('%s')" % name
        return self._save(sql)

    def retrieve_user_manual(self, user_manual_id=None, name=None):
        name, = self._convert_to_safetexts((name,))
        if user_manual_id is not None and name is not None:
            sql = "SELECT * FROM UserManual WHERE id=%s AND name='%s'" % (user_manual_id, name)
        elif user_manual_id is not None:
            sql = "SELECT * FROM UserManual WHERE id=%s" % (user_manual_id)
        elif name is not None:
            sql = "SELECT * FROM UserManual WHERE name='%s'" % (name)
        else:
            sql = "SELECT * FROM UserManual"
        return self._retrieve(sql)

    def save_source_sentence(self, text=None, section=None, page_num=None, user_manual_id=None):
        text, section = self._convert_to_safetexts((text, section))
        if section is None:
            section = ""
        if page_num is None:
            page_num = 0
        sql = "INSERT INTO SourceSentence (text, section, page_num, user_manual_id) VALUES ('%s', '%s', %s, %s)" % (text, section, page_num, user_manual_id)
        return self._save(sql)

    def retrieve_source_sentence(self, source_sentence_id=None, user_manual_id=None):
        if source_sentence_id is not None and user_manual_id is not None:
            sql = "SELECT * FROM SourceSentence WHERE id=%s AND user_manual_id='%s'" % (source_sentence_id, user_manual_id)
        elif source_sentence_id is not None:
            sql = "SELECT * FROM SourceSentence WHERE id=%s" % (source_sentence_id)
        elif user_manual_id is not None:
            sql = "SELECT * FROM SourceSentence WHERE user_manual_id='%s'" % (user_manual_id)
        else:
            sql = "SELECT * FROM SourceSentence"
        return self._retrieve(sql)

    def save_converted_sentence(self, source_sentence_id=None, roles=None):
        sql = "INSERT INTO ConvertedSentence (source_sentence_id"
        # decide columns and enter values
        values = []
        for k, v in roles.items():
            if k is None or v is None:
                continue
            sql += ","+k
            if v is None:
                v = ''
            elif type(v) is list:
                v = ' '.join(v)
            values.append(v)
        sql += ') VALUES (%s' % source_sentence_id
        for v in values:
            v, = self._convert_to_safetexts((v,))
            sql += ',"'+v+'"'
        sql += ')'
        return self._save(sql)

    def retrieve_converted_sentences(self, converted_sentence_id=None, source_sentence_id=None, roles=None):
        if converted_sentence_id is not None:
            sql = "SELECT * FROM ConvertedSentence WHERE id=%s" % (converted_sentence_id)
        elif source_sentence_id is not None and roles is not None and len(roles) > 0:
            sql = "SELECT * FROM ConvertedSentence WHERE source_sentence_id=%s" % (converted_sentence_id)
            for k, v in roles.items():
                sql += " AND %s=%s" % (k, v)
        elif source_sentence_id is not None and roles is not None and len(roles) > 0:
            sql = "SELECT * FROM ConvertedSentence WHERE source_sentence_id=%s" % (converted_sentence_id)
            for k, v in roles.items():
                sql += " AND %s=%s" % (k, v)
        elif source_sentence_id is not None:
            sql = "SELECT * FROM ConvertedSentence WHERE source_sentence_id=%s" % (converted_sentence_id)
        else:
            sql = "SELECT * FROM ConvertedSentence"
        return self._retrieve(sql)

    def _convert_to_safetexts(self, texts):
        return (str(text).replace("'", "''") if text is not None else 'None' for text in texts)

    def _save(self, sql):
        res = self.conn.execute(sql)
        self.conn.commit()
        return res.lastrowid

    def _retrieve(self, sql):
        cursor = self.conn.execute(sql)
        result = cursor.fetchall()
        return result

    def close(self):
        if self.conn is not None:
            self.conn.close()


if __name__ == '__main__':
    db = DBManager()
    db.drop_tables()
    db.create_tables()

