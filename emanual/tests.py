import logging
import unittest
from unittest import TestCase

import nltk
import requests

# Create your tests here.
from nltk import StanfordPOSTagger
from nltk import StanfordTokenizer
from nltk.parse.stanford import StanfordParser, StanfordDependencyParser

from emanual import constants
from emanual import database
from emanual import nlp
from emanual import settings

BASE_URL = "http://127.0.0.1:8000/api"



logging.basicConfig(
    format="[%(name)s][%(asctime)s] %(message)s",
    handlers=[logging.StreamHandler()],
    level=logging.INFO
)
logger = logging.getLogger(__name__)


@unittest.skip("")
class ConvertTestCase(TestCase):

    def test(self):
        res = requests.post("%s/convert" % BASE_URL, json={
            "action": "tokenize",
            "sentences": "How do I control the front-side mirror?"
        })
        logger.info(res.json())


@unittest.skip("")
class QueryTestCase(TestCase):

    def test(self):
        query = '''SELECT e."text", e.pos, e."role" FROM SentenceElement as e LEFT JOIN SourceSentence as s ON e.source_sentence_id = s.id WHERE e."role" = "{3}" AND s.id IN (
                        SELECT s1.id FROM SentenceElement as e1 LEFT JOIN SourceSentence as s1 ON e1.source_sentence_id = s1.id WHERE (e1."text" like "%%{1}%%" COLLATE NOCASE AND e1."role" = "{0}")
                        INTERSECT SELECT s2.id FROM SentenceElement as e2 LEFT JOIN SourceSentence as s2 ON e2.source_sentence_id = s2.id WHERE (e2."text" like "%%{2}%%" COLLATE NOCASE AND e2."role" = "Verb")
                        )'''

        query = query.format('Subject', 'system', 'prevent', 'Object')
        logger.info(query)

        db = database.DBManager()
        result = db._retrieve(query, ['text', 'pos', 'role'])
        logger.info(result)


@unittest.skip("")
class ParsingTestCase(TestCase):

    def test(self):

        parser = StanfordParser(path_to_jar=settings.STANFORDPARSER_PATH,
                                          path_to_models_jar=settings.STANFORDPARSER_MODEL_PATH)
        dep_parser = StanfordDependencyParser(path_to_jar=settings.STANFORDPARSER_PATH, path_to_models_jar=settings.STANFORDPARSER_MODEL_PATH)

        # sentences_str = "The vehicle has an electronic key release system. This system is to prevent ignition key removal."
        # sentences_str += " John likes Mary. He is 27 years old."
        # sentences_str += " Independent Mode : This mode directs rear seating airflow."
        sentences_str = """Starting the Vehicle
1) Press and release Q on the RKE transmitter.
2) Immediately, press and hold / for at least four seconds or until the parking lamps flash.
3) Start the vehicle normally after entering.
When the vehicle starts, the parking lamps will turn on.
Remote start can be extended.
Canceling a Remote Start
To cancel a remote start, do one of the following:
- Press and hold / until the parking lamps turn off.
- Turn on the hazard warning flashers.
- Turn the vehicle on and then off. See Remote Vehicle Start -> 31."""
        sentences_str_lines = sentences_str.split('\n')
        sentences = []
        for line in sentences_str_lines:
            sentences.extend(nltk.sent_tokenize(line))
        token_trees = parser.raw_parse_sents(sentences)

        # ----------------------------------
        # Tokenization
        # ----------------------------------
        def traverse(tree, elements, level=0):
            # rules of traversal stop
            label = tree.label()
            if (label != 'ROOT'
                and label != 'S' and  label != 'SBAR' and label != 'SBARQ'
                and label != 'SINV' and label != 'SQ'
                and label != 'VP'):
                if level > 1:
                    print(tree)
                    joined_text = (
                        ' '.join(tree.leaves())
                            .replace('-RRB-', ')')
                            .replace('-LRB-', '(')
                    )
                    elements.append([joined_text, label])  # (element, pos)
                    return

            for subtree in tree:
                if type(subtree) == nltk.tree.Tree:
                    traverse(subtree, elements, level=level+1)

        elements_list = []
        for sent, tree in zip(sentences, token_trees):
            print("-----------------------")
            print(sent)
            tree = next(tree)
            elements = []
            traverse(tree, elements)
            elements_list.append(elements)

        print("=================")

        # ----------------------------------
        # Dependency Parsing
        # ----------------------------------
        # her apple vs. her
        pronomial_words = ['this', 'those', 'these', 'the', 'that', 'he', 'she', 'him', 'her', 'it']
        dep_trees = dep_parser.raw_parse_sents(sentences)
        # iter sentences
        for idx_sentence, elements in enumerate(elements_list):
            tree = next(dep_trees)
            tree = next(tree)
            print("-----------------------")
            dep_results = [morpheme.split('\t') for morpheme in tree.to_conll(4).split('\n')]

            # iter the tokenized elements
            # an element may be a clause
            # the result of dependency parsing is a list of words
            # hence, need to split the clause
            idx_dep = 0
            for idx_element, element in enumerate(elements):
                words = element[0].split(' ')
                role = None
                pronomial = False
                # iter splited elements (word)
                for idx_word, word in enumerate(words):
                    # get matched dep result
                    dep_result = dep_results[idx_dep]
                    if dep_result[0] != word:
                        # not matched. skip
                        role = word
                        break
                    else:
                        idx_dep += 1

                    # the role is composed of [element, pos, head, dep]
                    # if the length of the dependency parsing result is 1, it may be the period
                    if len(dep_result) > 1:
                        pos = dep_result[1]
                        dep_tag = dep_result[3]
                    else:
                        pos = word
                        dep_tag = word
                    if 'subj' in dep_tag:
                        role = constants.ELEMENT_SUBJECT
                    elif 'obj' in dep_tag:
                        role = constants.ELEMENT_OBJECT
                    elif pos.startswith('VB') and dep_tag == 'aux':
                        role = constants.ELEMENT_AUXILIARYVERB
                    elif pos.startswith('VB'):
                        role = constants.ELEMENT_VERB
                    elif pos.startswith('RB'):
                        role = constants.ELEMENT_ADVERB
                    elif 'comp' in dep_tag or 'root' in dep_tag:
                        role = constants.ELEMENT_COMPLEMENT

                    # determine pronomial element
                    lower_word = word.lower()
                    if idx_word == 0 and lower_word in pronomial_words:
                        if lower_word == "her":
                            if len(words) > 1:
                                pronomial = True
                        else:
                            pronomial = True

                # add role of element
                elements_list[idx_sentence][idx_element].extend([role, pronomial])
        print(elements_list)


class NewConvertTestCase(TestCase):

    def test(self):
        res = requests.post("%s/convert" % BASE_URL, json={
            "action": "tokenize",
            "sentences": "John legend is married to Chrissy Teigen. The couple began dating in 2007 and he proposed in 2011. She is an american model who debuted in the Sports Illustrated swimsuit Issue."
        })
        logger.info(res.json())