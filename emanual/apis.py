import json
import logging

from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from emanual import settings, constants, nlp, database, clustering, inquiry

logging.basicConfig(
    format="[%(name)s][%(asctime)s] %(message)s",
    handlers=[logging.StreamHandler()],
    level=logging.INFO
)
logger = logging.getLogger(__name__)

MANUAL_ID = 1


@csrf_exempt
def handle_dbtable_retrival(request):
    try:
        if request.method == 'POST':
            db = database.DBManager()
            data = json.loads(request.body.decode('utf-8'))
            table_name = data.get('table')

            print(table_name)

            result = {}
            columns = []
            if table_name == 'SourceSentence':
                table = db.retrieve_source_sentence()
                columns = ['id', 'text', 'section', 'page_num', 'user_manual_id', 'timestamp']
            elif table_name == 'ConvertedSentence':
                table = db.retrieve_converted_sentences()
                columns = ['id', 'source_sentence_id', 'subject', 'verb', 'complement', 'object', 'sp', 'tp', 'mp', 'oc', 'ac', 'gc', 'wh', 'cj', 'object2', 'timestamp']
            # elif table_name == 'SentenceElement':
            #     table = db.retrieve_elements()
            # elif table_name == 'SentenceSchema':
            #     table = db.retrieve_sentence_schema()
            # elif table_name == 'Dependency':
            #     table = db.retrieve_dependencies()
            # elif table_name == 'PronounDependency':
            #     table = db.retrieve_pronoun_dependencies()
            db.close()
            if table is not None and type(table) is not list:
                table = [table]
            result['table'] = table
            result['columns'] = columns
            return JsonResponse(dict(constants.CODE_SUCCESS, **result))
        return JsonResponse(constants.CODE_FAILURE)
    except Exception as e:
        logger.exception(e)
        return JsonResponse(constants.CODE_FAILURE)


@csrf_exempt
def handle_convert(request):
    try:
        if request.method == 'POST':
            if len(request.body) == 0:
                return JsonResponse(constants.CODE_FAILURE)
            data = json.loads(request.body.decode('utf-8'))
            action = data.get('action')

            if action == 'tokenize':
                document = data.get('sentences')
                # sentences, tokens_list = nlp.analyze_document(document)
                sentences, tokens_list = nlp.parse_dependencies(document)
                roles = nlp.determine_roles(tokens_list)
                pronouns = nlp.find_pronouns(tokens_list)
                nouns = nlp.list_nouns(tokens_list)
                return JsonResponse(dict(constants.CODE_SUCCESS,
                                         **{'tokens': tokens_list, 'sentences': sentences, 'roles': roles,
                                            'pronouns': pronouns, 'nouns': nouns}))
            elif action == 'save':
                section = data.get('section', 0)
                sentences = data.get('sentences')
                converted_sentences = data.get('converted_sentences')

                db = database.DBManager()
                sentence_id_list = []
                element_id_list = {}  # sentence_id: [element_id, ...]
                for i in range(0, len(sentences)):
                    # source sentences
                    sentence_id = db.save_source_sentence(text=sentences[i], user_manual_id=MANUAL_ID, section=section)
                    sentence_id_list.append(sentence_id)
                    db.save_converted_sentence(sentence_id, converted_sentences[i])
                db.close()
                return JsonResponse(constants.CODE_SUCCESS)
            elif action == "save_backup":
                tokens = data.get('tokens')
                sentences = data.get('sentences')
                section = data.get('section')
                dependencies = data.get('dependencies')

                logger.debug(tokens)
                logger.debug(sentences)
                logger.debug(section)

                db = database.DBManager()
                sentence_id_list = []
                element_id_list = {}  # sentence_id: [element_id, ...]
                for i in range(0, len(sentences)):
                    # source sentences
                    sentence_id = db.save_source_sentence(text=sentences[i], user_manual_id=MANUAL_ID, section=section)
                    sentence_id_list.append(sentence_id)

                    # sentence elements
                    # _element_id_list = []
                    # for token in tokens[i]:
                    #     rowid = db.save_element(text=token[0], pos=token[1], role=token[2], source_sentence_id=sentence_id)
                    #     _element_id_list.append(rowid)
                    # element_id_list[sentence_id_list[-1]] = _element_id_list

                    roles = {}
                    for token in tokens[i]:
                        if token[-1] in roles:
                            roles[token[-1]] = roles[token[-1]]+";"+token[0]
                        else:
                            roles[token[-1]] = token[0]
                    # roles = {token[-1]: token[0] for token in tokens[i]}
                    db.save_converted_sentence(sentence_id, roles)

                    # sentence schema
                    # schema = str(tokens[i])
                    # db.save_sentence_schema(schema=schema, source_sentence_id=sentence_id)

                # dependency
                # for (sentence_from, element_from), (sentence_to, element_to) in dependencies:
                #     db.save_dependency(
                #         element_id_list[sentence_id_list[sentence_from]][element_from],
                #         element_id_list[sentence_id_list[sentence_to]][element_to]
                #     )
                for (converted_sentence_from, element_index_from, role_from), (converted_sentence_to, element_index_to, role_to) in dependencies:
                    db.save_pronoun_dependency(converted_sentence_from, role_from, converted_sentence_to, role_to)

                db.close()
                return JsonResponse(constants.CODE_SUCCESS)

        return JsonResponse(constants.CODE_FAILURE)
    except Exception as e:
        logger.exception(e)
        return JsonResponse(constants.CODE_FAILURE)


@csrf_exempt
def handle_rawquery(request):
    try:
        if request.method == 'POST':
            if len(request.body) == 0:
                return JsonResponse(constants.CODE_FAILURE)
            data = json.loads(request.body.decode('utf-8'))
            sql = data.get('query')
            db = database.DBManager()
            result = db._retrieve(sql)
            return JsonResponse(dict(constants.CODE_SUCCESS, **{'result': result}))
        return JsonResponse(constants.CODE_FAILURE)
    except Exception as e:
        logger.exception(e)
        return JsonResponse(constants.CODE_FAILURE)


@csrf_exempt
def _handle_query(request):
    try:
        if request.method == 'POST':
            if len(request.body) == 0:
                return JsonResponse(constants.CODE_FAILURE)
            data = json.loads(request.body.decode('utf-8'))
            query_elements = data.get('query')

            if constants.ELEMENT_SUBJECT in query_elements:
                format_params = {"noun_role": constants.ELEMENT_SUBJECT, "noun_text": query_elements[constants.ELEMENT_SUBJECT],
                                 "verb_text": query_elements[constants.ELEMENT_VERB],
                                 "result_role1": constants.ELEMENT_OBJECT,
                                 "result_role2": constants.ELEMENT_COMPLEMENT}
            elif constants.ELEMENT_OBJECT in query_elements:
                format_params = {"noun_role": constants.ELEMENT_OBJECT, "noun_text": query_elements[constants.ELEMENT_OBJECT],
                                 "verb_text": query_elements[constants.ELEMENT_VERB],
                                 "result_role1": constants.ELEMENT_SUBJECT,
                                 "result_role2": constants.ELEMENT_COMPLEMENT}
            else:
                format_params = {"noun_role": constants.ELEMENT_OBJECT, "noun_text": query_elements[constants.ELEMENT_OBJECT],
                                 "verb_text": query_elements[constants.ELEMENT_VERB],
                                 "result_role1": constants.ELEMENT_SUBJECT,
                                 "result_role2": constants.ELEMENT_OBJECT}

            query = """SELECT e."text", e.pos, e."role", s."text"
                FROM SentenceElement AS e LEFT JOIN SourceSentence AS s ON e.source_sentence_id = s.id
                WHERE (e."role" = "{result_role1}" OR e."role" = "{result_role2}") AND s.id IN (
                SELECT * FROM (SELECT s.id FROM SentenceElement AS e LEFT JOIN SourceSentence AS s ON e.source_sentence_id = s.id
                WHERE (e."text" LIKE "%{verb_text}%" COLLATE NOCASE AND e."role" = "Verb")
                INTERSECT
                SELECT s.id FROM SentenceElement AS e LEFT JOIN SourceSentence AS s ON e.source_sentence_id = s.id
                WHERE (e."text" LIKE "%{noun_text}%" COLLATE NOCASE AND e."role" = "{noun_role}"))
                UNION
                SELECT * FROM (SELECT s.id FROM SentenceElement AS e LEFT JOIN SourceSentence AS s ON e.source_sentence_id = s.id
                WHERE e."text" LIKE "%{verb_text}%" COLLATE NOCASE AND e."role" = "Verb"
                INTERSECT
                SELECT e.source_sentence_id FROM SentenceElement AS e
                WHERE e.id IN
                (SELECT d.element_from FROM Dependency AS d LEFT JOIN SentenceElement AS e ON d.element_to = e.id
                WHERE e."text" LIKE "%{noun_text}%" COLLATE NOCASE))
                )"""

            query = query.format(**format_params)

            db = database.DBManager()
            result = db._retrieve(query, ['text', 'pos', 'role', 'sentence'])
            if type(result) is dict:
                result = [result]
            db.close()

            return JsonResponse(dict(constants.CODE_SUCCESS, **{'result': result}))
        return JsonResponse(constants.CODE_FAILURE)
    except Exception as e:
        logger.exception(e)
        return JsonResponse(constants.CODE_FAILURE)


@csrf_exempt
def handle_manage(request):
    if request.method == 'GET':
        action = request.GET.get('action')
        if not action:
            return JsonResponse(constants.CODE_FAILURE)

        if action == 'create':
            manual_name = request.GET.get('manual_name')
            if not manual_name:
                manual_name = '2016 GMC ACADIA OWNERS MANUAL'
            db = database.DBManager()
            db.create_tables()
            db.save_user_manual(name=manual_name)
            rs = db.retrieve_user_manual(name=manual_name)
            db.close()
            return JsonResponse(dict(constants.CODE_SUCCESS, **{'result': rs}))
        elif action == 'manual':
            db = database.DBManager()
            rs = db.retrieve_user_manual()
            db.close()
            return JsonResponse(dict(constants.CODE_SUCCESS, **{'result': rs}))
        elif action == 'sentence':
            db = database.DBManager()
            manual_id = request.GET.get('manual_id')
            if not manual_id:
                manual_id = 1
            sentence_id = request.GET.get('sentence_id')
            # if not sentence_id:
            #     sentence_id = None
            rs = db.retrieve_source_sentence(user_manual_id=manual_id, source_sentence_id=sentence_id)
            db.close()
            return JsonResponse(dict(constants.CODE_SUCCESS, **{'result': rs}))
        elif action == 'schema':
            db = database.DBManager()
            sentence_id = request.GET.get('sentence_id')
            # if not sentence_id:
            #     sentence_id = None
            schema_id = request.GET.get('schema_id')
            # if not schema_id:
            #     schema_id = None
            rs = db.retrieve_sentence_schema(source_sentence_id=sentence_id, schema_id=schema_id)
            db.close()
            return JsonResponse(dict(constants.CODE_SUCCESS, **{'result': rs}))

    return JsonResponse(constants.CODE_FAILURE)