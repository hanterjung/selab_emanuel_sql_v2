import json
import logging

import collections
import nltk
from nltk.corpus import wordnet as wn
from nltk.parse.stanford import StanfordParser, StanfordDependencyParser

from emanual import constants
from emanual import settings
from emanual import database

logging.basicConfig(
    format="[%(name)s][%(asctime)s] %(message)s",
    handlers=[logging.StreamHandler()],
    level=logging.DEBUG
)
logger = logging.getLogger(__name__)

SP_LIST = (
    'after',
    'among',
    'at',
    'behind',
    'in',
    'in front',
    'of',
    'next to',
    'beside',
    'by',
    'on',
    'over',
    'above',
    'under',
    'below'
)

TP_LIST = (
    'at',
    'on',
    'by',
    'till',
    'until',
    'up',
    'since',
    'in',
    'at',
    'during',
    'for',
    'throughout',
    'while'
)

MP_LIST = (
    'to',
    'through',
    'across',
    'along',
    'down',
    'over',
    'off',
    'round',
    'into',
    'inside'
)

OC_LIST = (
    'about',
)

AC_LIST = (
    'with',
    'along',
    'without'
)

GC_LIST = (
    'for',
    'to'
)

WH_LIST = (
    'when',
    'where',
    'who',
    'which'
)

CJ_LIST = (
    'if',
    'because',
    'whether',
    'but',
    'yet'
)


def _traverse(tree, elements, level=0):
    # rules of traversal stop
    label = tree.label()
    if (label != 'ROOT'
        and label != 'S' and  label != 'SBAR' and label != 'SBARQ'
        and label != 'SINV' and label != 'SQ'
        and label != 'PP' and label != 'VP'
        and label != 'FRAG'):
        logger.debug(tree)
        joined_text = (
            ' '.join(tree.leaves())
                .replace('-RRB-', ')')
                .replace('-LRB-', '(')
        )
        elements.append([joined_text, label])  # (element, pos)
        return

    for subtree in tree:
        if type(subtree) == nltk.tree.Tree:
            _traverse(subtree, elements, level=level + 1)


def analyze_document(document):
    # --------------------------------------
    # example sentences
    # --------------------------------------
    # sentences_str = "The vehicle has an electronic key release system. This system is to prevent ignition key removal."
    # sentences_str += " John likes Mary. He is 27 years old."
    # sentences_str += " Independent Mode : This mode directs rear seating airflow."

    parser = StanfordParser(path_to_jar=settings.STANFORDPARSER_PATH, path_to_models_jar=settings.STANFORDPARSER_MODEL_PATH)
    dep_parser = StanfordDependencyParser(path_to_jar=settings.STANFORDPARSER_PATH, path_to_models_jar=settings.STANFORDPARSER_MODEL_PATH)

    document_lines = document.split('\n')
    sentences = []
    for line in document_lines:
        sentences.extend(nltk.sent_tokenize(line))
    token_trees = parser.raw_parse_sents(sentences)

    # ----------------------------------
    # Sentence Tokenization
    # sentence to elements
    # ----------------------------------
    elements_list = []
    for sent, tree in zip(sentences, token_trees):
        logger.debug("-----------------------")
        logger.debug("Sentence: %s" % (sent))
        tree = next(tree)
        elements = []
        _traverse(tree, elements)
        elements_list.append(elements)

    # ----------------------------------
    # Dependency Parsing
    # ----------------------------------
    # her apple vs. her
    pronomial_words = ['this', 'those', 'these', 'the', 'that', 'he', 'she', 'him', 'her', 'it']
    dep_trees = dep_parser.raw_parse_sents(sentences)
    # iter sentences
    for idx_sentence, elements in enumerate(elements_list):
        tree = next(dep_trees)
        tree = next(tree)
        logger.debug("-----------------------")
        dep_results = [morpheme.split('\t') for morpheme in tree.to_conll(4).split('\n')]

        # iter the tokenized elements
        # an element may be a clause
        # the result of dependency parsing is a list of words
        # hence, need to split the clause
        idx_dep = 0
        for idx_element, element in enumerate(elements):
            words = element[0].split(' ')
            role = None
            pronomial = False
            # iter splited elements (word)
            for idx_word, word in enumerate(words):
                # get matched dep result
                dep_result = dep_results[idx_dep]
                if dep_result[0] != word:
                    # not matched. skip
                    role = word
                    break
                else:
                    idx_dep += 1

                # the role is composed of [element, pos, head, dep]
                # if the length of the dependency parsing result is 1, it may be the period
                if len(dep_result) > 1:
                    pos = dep_result[1]
                    dep_tag = dep_result[3]
                else:
                    pos = word
                    dep_tag = word
                if 'subj' in dep_tag:
                    role = constants.ELEMENT_SUBJECT
                elif 'obj' in dep_tag:
                    role = constants.ELEMENT_OBJECT
                elif pos.startswith('VB') and dep_tag == 'aux':
                    role = constants.ELEMENT_AUXILIARYVERB
                elif pos.startswith('VB'):
                    role = constants.ELEMENT_VERB
                elif pos.startswith('RB'):
                    role = constants.ELEMENT_ADVERB
                elif 'comp' in dep_tag or 'root' in dep_tag:
                    role = constants.ELEMENT_COMPLEMENT

                # determine pronomial element
                lower_word = word.lower()
                if idx_word == 0 and lower_word in pronomial_words:
                    if lower_word == "her":
                        if len(words) > 1:
                            pronomial = True
                    else:
                        pronomial = True

            # add role of element
            elements_list[idx_sentence][idx_element].extend([role, pronomial])
    logger.debug("Result: %s" % (elements_list))
    return sentences, elements_list


def parse_dependencies(document):
    dep_parser = StanfordDependencyParser(path_to_jar=settings.STANFORDPARSER_PATH,
                                          path_to_models_jar=settings.STANFORDPARSER_MODEL_PATH)

    document_lines = document.split('\n')
    sentences = []
    for line in document_lines:
        sentences.extend(nltk.sent_tokenize(line))

    words_list = []
    for sent in sentences:
        words_list.append(nltk.word_tokenize(sent))

    results = []
    dep_trees = dep_parser.raw_parse_sents(sentences)
    for trees in dep_trees:
        for tree in trees:
            results.append([morpheme.split('\t') for morpheme in tree.to_conll(4).split('\n')])

    fixed_results = []
    for result, words in zip(results, words_list):
        fixed_result = []
        result_idx = 0
        for w in words:

            result_word = None
            if result_idx < len(result):
                result_word = result[result_idx]
                if len(result_word) < 2:
                    result_idx += 1

            if w == result_word[0]:
                fixed_result.append(result_word)
                result_idx += 1
            else:
                fixed_result.append([w, None, None, None])
        fixed_results.append(fixed_result)

    return sentences, fixed_results


def determine_roles(elements_list):
    tables = []
    for elements in elements_list:
        table = collections.OrderedDict([
            ("subject", None),
            ("verb", None),
            ("complement", None),
            ("object", None),
            ("sp", None),
            ("tp", None),
            ("mp", None),
            ("oc", None),
            ("ac", None),
            ("gc", None),
            ("wh", None),
            ("cj", None),
            ("object2", None)
        ])
        mark_subj = False
        mark_verb = False
        mark_obj = False
        mark_obj2 = False
        vbz = False
        object2_wlist = []
        aux_wlist = []
        mod_wlist = []
        subj_dup = False
        verb_dup = False
        for i, el in enumerate(elements):
            word = el[0]
            word_lower = word.lower()
            pos = el[1]
            dep = el[3]

            if mark_obj2:
                if word != '.':
                    if (dep is not None and 'subj' in dep) and pos != 'WP':
                        if subj_dup:
                            subj_dup = False
                            mark_obj2 = False
                        else:
                            subj_dup = True
                    elif pos is not None and pos.startswith('VB'):
                        if verb_dup:
                            verb_dup = False
                            mark_obj2 = False
                        else:
                            verb_dup = True
                    if mark_obj2:
                        object2_wlist.append(i)
                        continue
                else:
                    continue

            if dep is not None and (dep == 'mark' and pos != 'IN'):
                mark_subj = True
                mark_verb = True
                mark_obj = True

            if (dep is not None and (('mod' in dep or dep == 'compound') and (pos != 'WRB' and pos != 'WDT' and pos != 'WP'))) or pos == 'DT' or pos == 'POS':
                mod_wlist.append(i)
                continue

            if (dep is not None and 'aux' in dep) or pos == 'MD':
                aux_wlist.append(i)
                continue

            if (dep is not None and 'subj' in dep) and pos != 'WP':
                if mark_subj:
                    mark_subj = False
                    object2_wlist.extend(mod_wlist)
                    object2_wlist.append(i)
                    mod_wlist = []
                else:
                    table['subject'] = mod_wlist + [i]
                    mod_wlist = []
                    mark_subj = False
                    mark_verb = False
                    mark_obj = False
            elif pos is not None and pos.startswith('VB'):
                if mark_verb:
                    mark_verb = False
                    object2_wlist.extend(mod_wlist)
                    object2_wlist.append(i)
                    mod_wlist = []
                elif pos == 'VBG' and (mark_subj or mark_obj):
                    object2_wlist.extend(mod_wlist)
                    object2_wlist.append(i)
                    mod_wlist = []
                else:
                    if table['verb'] is None:
                        table['verb'] = aux_wlist + [i]
                        aux_wlist = []
                        if len(mod_wlist) > 0 and table['subject'] is None:
                            table['subject'] = mod_wlist.copy()
                            mod_wlist = []
                    else:
                        table['verb'] = aux_wlist + [i]
                        aux_wlist = []
                    if pos == 'VBZ':
                        vbz = True
            elif dep is not None and (vbz and dep == 'root'):
                table['complement'] = mod_wlist + [i]
                mod_wlist = []
            elif dep is not None and 'obj' in dep:
                if mark_obj:
                    mark_obj = False
                    object2_wlist.extend(mod_wlist)
                    object2_wlist.append(i)
                    mod_wlist = []
                else:
                    table['object'] = mod_wlist + [i]
                    mod_wlist = []
                    mark_subj = False
                    mark_verb = False
                    mark_obj = False
            elif word_lower in GC_LIST and not mark_obj:
                table['gc'] = [i]
                mark_obj = True
                mark_obj2 = True
            elif word_lower in SP_LIST and not mark_obj:
                table['sp'] = [i]
                mark_obj = True
                mark_obj2 = True
            elif word_lower in TP_LIST and not mark_obj:
                table['tp'] = [i]
                mark_obj = True
                mark_obj2 = True
            elif word_lower in MP_LIST and not mark_obj:
                table['mp'] = [i]
                mark_obj = True
                mark_obj2 = True
            elif word_lower in OC_LIST and not mark_obj:
                table['oc'] = [i]
                mark_obj = True
                mark_obj2 = True
            elif word_lower in AC_LIST and not mark_obj:
                table['ac'] = [i]
                mark_obj = True
                mark_obj2 = True
            elif pos is not None and (pos == 'WDT' or pos == 'WP' or pos == 'WRB') and not mark_obj:
                table['wh'] = [i]
                mark_subj = True
                mark_verb = True
                mark_obj = True
                mark_obj2 = True
            elif word_lower in SP_LIST and not mark_obj:
                table['cj'] = [i]
                mark_obj = True
            else:
                if word != '.':
                    object2_wlist.extend(mod_wlist)
                    object2_wlist.append(i)
                    mod_wlist = []
                else:
                    object2_wlist.extend(mod_wlist)
                    mod_wlist = []

        object2_wlist.extend(mod_wlist)
        table['object2'] = object2_wlist
        tables.append(table)
    return tables


def find_pronouns(elements_list):
    pronouns_list = []
    for elements in elements_list:
        pronouns = []
        for i, el in enumerate(elements):
            word = el[0]
            pos = el[1]
            if pos is None:
                continue
            # if pos == 'DT':
            #     pronoun_index = int(el[2])
            #     pronouns.append((i, pronoun_index-1))
            if word.lower() in ['this', 'these', 'those', 'i', 'you', 'he', 'she', 'it', 'we', 'they', 'them', 'me',
                                'your', 'his', 'her', 'its', 'our', 'their', 'yours', 'his',
                                'hers', 'ours', 'theirs', 'myself', 'yourself', 'herself', 'himself',
                                'Itself', 'ourselves', 'yourselves', 'themselves']:
                pronouns.append((i,))
            elif word.lower() == 'that' and pos == 'DT':
                pronouns.append((i,))
            elif pos.startswith('PRP'):
                pronouns.append((i,))
        pronouns_list.append(pronouns)
    return pronouns_list


def list_nouns(elements_list):
    nouns = []
    for elements in elements_list:
        for el in elements:
            if el[1] is not None and el[1].startswith('NN'):
                nouns.append(el[0])
    return list(set(nouns))


if __name__ == '__main__':
    document = """The Safety Canopy is mounted to the roof side-rail sheet metal that is behind the headliner."""
    document = """The manual has information about components, controls and indicators."""
    document = """John gave a pen to Mary."""
    document = """The manual has information about components, controls and indicators. It is used for easier navigation of information. The sensing system warns the driver of obstacles within certain areas. The Safety Canopy is mounted to the roof side-rail sheet metal. The root side rail-sheet metal is behind the headliner."""
    document = """John legend is married to Chrissy Teigen. The couple began dating in 2007 and he proposed in 2011. She is an american model who debuted in the Sports Illustrated swimsuit Issue."""
    document = """She is an american model who debuted in the Sports Illustrated swimsuit Issue."""
    document = """My name is John."""
    document = """The couple began dating in 2007 and he proposed in 2011."""
    document = """While I was driving home, I saw an accident."""
    document = """Google Brain touches nearly every piece of its business such as translation, photography and advertising."""
    document = """That is an apple."""
    document = """This systems team has been doing a great job for building a foundation."""
    document = """Trump was voted as America's president. He was inaugurated in Januray of 2017."""
    document = """John's vehicle had some engine problems. They were reoccurring for months."""
    document = """Sally purchased converse shoes from Nordstom. They were only $20."""
    document = """The design group decided to go on a light picnic. It will be in Santa monica pier."""
    document = """Michelle was studying when she heard the noise."""

    elements_list = parse_dependencies(document)
    tables = determine_roles(elements_list[1])
    pronouns = find_pronouns(elements_list[1])
    print(elements_list)
    print(json.dumps(tables, indent=4))
    print(pronouns)
    exit()

    document = """The vehicle has an electronic key release system. This system is to prevent ignition key removal."""
    elements_list = parse_dependencies(document)
    exit()

    document = """The vehicle has an electronic key release system. This system is to prevent ignition key removal."""
    document1 = """If equipped, the engine can be started from outside of the vehicle.
Starting the Vehicle
1) Press and release Q on the RKE transmitter.
2) Immediately, press and hold / for at least four seconds or until the parking lamps flash.
3) Start the vehicle normally after entering.
When the vehicle starts, the parking lamps will turn on.
Remote start can be extended.
Canceling a Remote Start
To cancel a remote start, do one of the following:
- Press and hold / until the parking lamps turn off.
- Turn on the hazard warning flashers.
- Turn the vehicle on and then off. See Remote Vehicle Start -> 31."""
    document2 = """To lock or unlock a door manually:
- From the inside use the door lock knob on the window sill.
- From the outside turn the key toward the front or rear of the vehicle, or press the K or Q button on the Remote Keyless Entry (RKE) transmitter.
Power Door Locks
K : Press to unlock the doors.
Q : Press to lock the doors. See Power Door Locks -> 33."""
    document3 = """To open the liftgate the vehicle must be in P (Park). Press the touch pad under the liftgate handle and lift up. To close the liftgate, use the pull cup or pull strap as an aid.
Power Liftgate
If equipped with a power liftgate, the vehicle must be in P (Park) to operate it.
- Press and hold 8 on the Remote Keyless Entry (RKE) transmitter.
- Press O.
- Press the touch pad on the outside liftgate handle.
See Liftgate -> 35."""

    analyze_document(document)