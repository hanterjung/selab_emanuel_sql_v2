import ast

import emanual.database as db

dbman = db.DBManager()
source_sentence_list = dbman.retrieve_source_sentence()
for source_sentence in source_sentence_list:
    schema = dbman.retrieve_sentence_schema(source_sentence_id=source_sentence['id'])
    for token in ast.literal_eval(schema['text']):
        pos = None
        if len(token) >= 4:
            pos = token[3]
        dbman.save_element(text=token[0], pos=pos, source_sentence_id=source_sentence['id'])
